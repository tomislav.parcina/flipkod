<?php

namespace App\Service;

use App\Entity\Circle;
use App\Entity\Triangle;

class GeometryCalculator
{
    public function getTotalSurface(Triangle $triangle, Circle $circle): float
    {
        return $triangle->getSurface() + $circle->getSurface();
    }

    public function getTotalCircumference(Triangle $triangle, Circle $circle): float
    {
        return $triangle->getCircumference() + $circle->getCircumference();
    }
}
