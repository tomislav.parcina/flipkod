<?php

namespace App\Controller;

use App\Entity\Circle;
use App\Entity\Triangle;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShapeController extends AbstractController
{
    /**
     * @Route("/circle/{radius}")
     */    
    public function getCircle(float $radius): Response
    {
        $circle = new Circle($radius);
        return $this->json($circle->getJSONParameters());
    }

    /**
     * @Route("/triangle/{a}/{b}/{c}")
     */    
    public function getTriangle(float $a, float $b, float $c): Response
    {
        $triangle = new Triangle($a, $b, $c);
        return $this->json($triangle->getJSONParameters($a, $b, $c));
    }
}
