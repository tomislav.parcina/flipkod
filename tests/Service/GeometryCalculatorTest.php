<?php

namespace App\Tests\Service;

use App\Service\GeometryCalculator;
use PHPUnit\Framework\TestCase;
use App\Entity\Circle;
use App\Entity\Triangle;

class GeometryCalculatorTest extends TestCase
{
	/**
	 * @testWith [2, 2, 3, 4, 15.47]
	 *	         [3, 4, 5, 6, 38.19]
	 */
    public function testTotalSurface(float $radius, float $a, float $b, float $c, float $surface): void
    {
    	$circle = new Circle($radius);
    	$triangle = new Triangle($a, $b, $c);
        $geometryCalculator = new GeometryCalculator();
        $this->assertEquals(
        	$surface, 
        	$geometryCalculator->getTotalSurface($triangle, $circle),
        	'Izračunata ukupna površina kruga i trokuta nije ispravna!'
        );
    }

	/**
	 * @testWith [2, 2, 3, 4, 21.57]
	 *	         [3, 4, 5, 6, 33.85]
	 */
    public function testTotalCircumference(float $radius, float $a, float $b, float $c, float $surface): void
    {
    	$circle = new Circle($radius);
    	$triangle = new Triangle($a, $b, $c);
        $geometryCalculator = new GeometryCalculator();
        $this->assertEquals(
        	$surface, 
        	$geometryCalculator->getTotalCircumference($triangle, $circle),
        	'Izračunati ukupni opseg kruga i trokuta nije ispravna!'
        );
    }
}
